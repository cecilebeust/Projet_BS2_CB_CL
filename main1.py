# 1.2.1
from array import array
import numpy as np


def read_interaction_file_dict(file: str) -> dict:
    """AI is creating summary for read_interaction_file_dict

    Args:
        file (str): [description]

    Returns:
        dict: [description]
    """
    dico = {}
    with open(file, "r") as fi:
        compt_line = 0
        for i in fi:
            if compt_line > 0:
                line_read = i.strip().split(" ")
                if line_read[0] in dico:
                    dico[line_read[0]].append(line_read[1])
                else:
                    dico[line_read[0]] = [line_read[1]]
                if line_read[1] in dico:
                    dico[line_read[1]].append(line_read[0])
                else:
                    dico[line_read[1]] = [line_read[0]]
            compt_line += 1
    return dico


# 1.2.2


def read_interaction_file_list(file: str) -> list:
    """AI is creating summary for read_interaction_file_list

    Args:
        file (str): [description]

    Returns:
        list: [description]
    """
    liste = []
    with open(file, "r") as fi:
        compt_line = 0
        for i in fi:
            if compt_line > 0:
                line_read = i.strip().split(" ")
                liste.append((line_read[0], line_read[1]))
            compt_line += 1
    liste = list(set(liste))
    return liste


# 1.2.3


def read_interaction_file_mat(file: str) -> array:
    """AI is creating summary for read_interaction_file_mat

    Args:
        file (str): [description]

    Returns:
        array: [description]
    """
    dico = {}
    with open(file, "r") as fi:
        compt_line = 0
        for i in fi:
            if compt_line > 0:
                line_read = i.strip().split(" ")
                if line_read[0] not in dico:
                    dico[line_read[0]] = len(dico)
                if line_read[1] not in dico:
                    dico[line_read[1]] = len(dico)
            else:
                compt_line = 1
    matrice = np.array([[0]*len(dico)]*len(dico))

    compt_line = 0
    with open(file, "r") as fi:
        for j in fi:
            if compt_line > 0:
                line_read = j.strip().split(" ")
                matrice[dico[line_read[0]]][dico[line_read[1]]] = 1
                matrice[dico[line_read[1]]][dico[line_read[0]]] = 1
            compt_line = 1

    return matrice


# 1.2.4


def read_interaction_file(file: str) -> tuple:
    """AI is creating summary for read_interaction_file

    Args:
        file (str): [description]

    Returns:
        tuple: [description]
    """
    l_som = []
    with open(file, "r") as fi:
        compt_line = 0
        for i in fi:

            if compt_line > 0:
                line_read = i.split(" ")
                if line_read[0] not in l_som:
                    l_som.append(line_read[0])
                if line_read[1].strip() not in l_som:
                    l_som.append(line_read[1].strip())
            compt_line = 1
    return (read_interaction_file_dict(file), read_interaction_file_list(file), read_interaction_file_mat(file), l_som)


# 1.2.5
# ??

# 1.2.6

# 1.2.7


def is_interaction_file(file: str) -> bool:
    """AI is creating summary for is_interaction_file

    Args:
        file (str): [description]

    Returns:
        bool: [description]
    """
    with open(file, "r") as fi:
        if len(list(fi)) == 0:
            return False
        compt_line = 0
        for i in fi:
            if compt_line > 0:
                if len(i.strip().split("\t")) != 2:
                    return False
            else:
                if not i.strip().isdigit():
                    return False
                compt_line = 1
    return True


# 2.1
# 2.1.1


def count_vertices(file: str) -> int:
    """AI is creating summary for count_vertices

    Args:
        file (str): [description]

    Returns:
        int: [description]
    """
    return len(read_interaction_file_dict(file))


# 2.1.2


def count_edges(file: str) -> int:
    """AI is creating summary for count_edges

    Args:
        file (str): [description]

    Returns:
        int: [description]
    """
    return len(read_interaction_file_list(file))


# 2.1.3
def clean_interactome(filein: str, fileout: str) -> str:
    """AI is creating summary for clean_interactome

    Args:
        filein (str): [description]
        fileout (str): [description]

    Returns:
        str: [description]
    """
    liste = []
    with open(filein, "r") as fi:
        compt_line = 0
        for i in fi:
            if compt_line > 0:
                line_read = i.strip().split("\t")
                if line_read[0] != line_read[1]:
                    liste.append((line_read[0], line_read[1]))
            compt_line += 1
    liste = list(set(liste))
    with open(fileout, "a") as fo:
        fo.write(str(len(liste))+"\n")
        for i in liste:
            fo.write(i[0]+"\t"+i[1]+"\n")
    return fileout

# 2.2.1


def get_degree(file, prot) -> int:
    """AI is creating summary for get_degree

    Args:
        file ([type]): [description]
        prot ([type]): [description]

    Returns:
        [type]: [description]
    """
    counter = 0
    with open(file, "r") as fi:
        for i in fi:
            if prot in i:
                counter += 1
    return counter


# 2.2.2

def get_max_degree(file):
    """AI is creating summary for get_max_degree

    Args:
        file ([type]): [description]

    Returns:
        [type]: [description]
    """
    dico = read_interaction_file_dict(file)
    dico2 = {}
    for i in dico:
        dico2[i] = get_degree(file, i)
    max = 0
    for j in dico2:
        if dico2[j] > max:
            max_vertices = j
            max = dico2[j]
    return max_vertices


# 2.2.3
def get_ave_degree(file):
    """AI is creating summary for get_ave_degree

    Args:
        file ([type]): [description]

    Returns:
        [type]: [description]
    """
    dico = read_interaction_file_dict(file)
    somme = 0
    for i in dico:
        somme += get_degree(file, i)
    return somme/len(dico)


# 2.2.4
def count_degree(file, deg):
    """AI is creating summary for count_degree

    Args:
        file ([type]): [description]
        deg ([type]): [description]

    Returns:
        [type]: [description]
    """
    dico = read_interaction_file_dict(file)
    dico2 = {}
    counter = 0
    for i in dico:
        dico2[i] = get_degree(file, i)
    for j in dico2:
        if dico2[j] == deg:
            counter += 1
    return counter

# 2.2.5


def histogram_degree(file, dmin, dmax):
    """AI is creating summary for histogram_degree

    Args:
        file ([type]): [description]
        dmin ([type]): [description]
        dmax ([type]): [description]
    """
    dico = read_interaction_file_dict(file)
    dico2 = {}
    delete = list()
    for i in dico:
        dico2[i] = get_degree(file, i)
    for j in dico2:
        if dico2[j] < dmin or dico2[j] > dmax:
            delete.append(j)
    for k in delete:
        dico2.pop(k)

    for l in dico2:
        print(l + " " + "*"*dico2[l])

# 3.1.1
# On lit 2 fois le fichier, une fois pour créer le dico et une autre
# pour obtenir le degré des protéines


class Interactome:
    def __init__(self, file):
        self.read_file = read_interaction_file(file)
        self.__int_dic__ = self.read_file[0]
        self.__int_list__ = self.read_file[1]
        self.__int_mat__ = self.read_file[2]

    # 2.1
    # 2.1.1

    def count_vertices(self) -> int:
        """AI is creating summary for count_vertices

        Args:
            file (str): [description]

        Returns:
            int: [description]
        """
        return len(self.__int_dic__)

    # 2.1.2

    def count_edges(self) -> int:
        """AI is creating summary for count_edges

        Args:
            file (str): [description]

        Returns:
            int: [description]
        """
        return len(self.__int_list__)

    # 2.2.1

    def get_degree(self, prot) -> int:
        """AI is creating summary for get_degree

        Args:
            file ([type]): [description]
            prot ([type]): [description]

        Returns:
            [type]: [description]
        """
        return len(self.__int_dic__[prot])

    # 2.2.2

    def get_max_degree(self):
        """AI is creating summary for get_max_degree

        Args:
            file ([type]): [description]

        Returns:
            [type]: [description]
        """
        dico2 = {}
        for i in self.__int_dic__:
            dico2[i] = get_degree(i)
        max = 0
        for j in dico2:
            if dico2[j] > max:
                max_vertices = j
                max = dico2[j]
        return max_vertices

    # 2.2.3

    def get_ave_degree(self) -> int:
        """AI is creating summary for get_ave_degree

        Args:
            file ([type]): [description]

        Returns:
            [type]: [description]
        """
        somme = 0
        for i in self.__int_dic__:
            somme += get_degree(i)
        return somme/len(self.__int_dic__)

    # 2.2.4

    def count_degree(self, deg) -> int:
        """AI is creating summary for count_degree

        Args:
            file ([type]): [description]
            deg ([type]): [description]

        Returns:
            [type]: [description]
        """
        dico2 = {}
        counter = 0
        for i in self.__int_dic__:
            dico2[i] = get_degree(i)
        for j in dico2:
            if dico2[j] == deg:
                counter += 1
        return counter

    # 2.2.5

    def histogram_degree(self, dmin, dmax):
        """AI is creating summary for histogram_degree

        Args:
            file ([type]): [description]
            dmin ([type]): [description]
            dmax ([type]): [description]
        """
        dico2 = {}
        delete = list()
        for i in self.__int_dic__:
            dico2[i] = get_degree(i)
        for j in dico2:
            if dico2[j] < dmin or dico2[j] > dmax:
                delete.append(j)
        for k in delete:
            dico2.pop(k)

        for l in dico2:
            print(l + " " + "*"*dico2[l])

    def density(self) -> float:
        """AI is creating summary for density

        Returns:
            float: [description]
        """
        return ((2*self.count_edges())/(self.count_vertices()*(self.count_vertices()-1)))

    def clustering(self):
        pass


int1 = Interactome("toy_example.txt")

print(int1.density())
