import matplotlib.pyplot as plt
import networkx as nx

import numpy as np
import random


def graph_generator_barabasy(n0: int = 2, nsup: int = 1):
    matrice = np.array([[1]*n0]*n0)
    for i in range(n0):
        matrice[i][i] = 0

    for _ in range(nsup):
        new_axis = [0]*(len(matrice[0]))
        for j in range(len(matrice[0])):
            if random.random() < (sum(matrice[j]/len(matrice[0]))):
                new_axis[j] = 1

        new_col = new_axis.copy()
        new_col.append(0)
        new_col = np.array(new_col)[:, np.newaxis]

        new_axis = np.array(new_axis)[np.newaxis, :]

        matrice = np.concatenate((matrice, new_axis))
        matrice = np.concatenate((matrice, new_col), axis=1)

    return matrice


mat = graph_generator_barabasy(n0=2, nsup=10)
print(mat)
mylabels = ["A", "B", "C", "D", "E"]


def show_graph_with_labels(adjacency_matrix, mylabels):
    rows, cols = np.where(adjacency_matrix == 1)
    edges = zip(rows.tolist(), cols.tolist())
    gr = nx.Graph()
    gr.add_edges_from(edges)
    nx.draw(gr, pos=nx.circular_layout(gr),
            node_size=500, labels=None, with_labels=True)
    plt.show()


show_graph_with_labels(mat, mylabels)
