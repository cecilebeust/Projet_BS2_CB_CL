'''
Projet UE BS2 - Master 2 Bioinformatique
parcours IBI

Corentin Lucas
Cécile Beust

TEST FILE
'''
import pytest
import os
from main import *
import numpy as np
import unittest

# 1.2.7

def is_interaction_file(filename:str) -> None:
    """Tests if the input file has the right format

    Args:
        filename (str): the name of the input file
    
    Returns:
        None
    """
    # Check if the file is empty
    assert os.stat(filename).st_size != 0
    with open(filename, 'r') as fh:
        lines = fh.readlines()
        # Check if the first line is a number
        first_line = lines[0].strip()
        assert first_line.isdigit() == True
        # Check if the first line represents the number of interactions
        assert len(lines)-1 == int(first_line)
        # Check if the file contains the right column numbers
        line_counter:int = 0
        for line in lines : 
            if line_counter > 0:
                assert len(line.split(" ")) == 2
            line_counter += 1

'''print("TOY EXAMPLE")
is_interaction_file("toy_example.txt")

print("EMPTY FILE")
is_interaction_file("empty_file.txt")

print("FILE WO FIRST LINE")
is_interaction_file("file_wo_first_line.txt")

print("FILE WITH WRONG NUMBER")
is_interaction_file("file_with_wrong_number.txt")

print("FILE WITH WRONG COLUMN")
is_interaction_file("file_with_wrong_number.txt")'''

is_interaction_file("test_interactome_file.txt")

def test_read_interaction_dict() -> None:
    """Tests the function read_interaction_file_dict

    Returns:
        None
    """
    dico_interactions = read_interaction_file_dict("test_interactome_file.txt")
    assert type(dico_interactions) == dict
    assert dico_interactions == {'prot1': ['prot2', 'prot3', 'prot4', 'prot5'], \
        'prot2': ['prot1', 'prot3', 'prot4', 'prot5', 'prot6'], \
            'prot3': ['prot2', 'prot1', 'prot4', 'prot6'], \
                'prot4': ['prot2', 'prot1', 'prot3'], \
                    'prot5': ['prot2', 'prot1', 'prot6'], \
                        'prot6': ['prot5', 'prot2', 'prot3']}
    assert len(dico_interactions.keys()) == 6
    seen_protein = []
    for protein in dico_interactions.keys():
        # Check if there are no duplicates in the dico
        assert protein not in seen_protein
        # assert the type str
        assert type(protein) == str
        seen_protein.append(protein)
        for interacting_proteins in dico_interactions.values():
            assert type(interacting_proteins) == list

test_read_interaction_dict()

def test_read_interaction_file_list() -> None:
    """Tests the function read_interaction_file_list
    
    Returns:
        None
    """
    list_interactions = read_interaction_file_list("test_interactome_file.txt")
    assert type(list_interactions) == list
    assert list_interactions == [('prot1', 'prot2'), ('prot2', 'prot3'), ('prot1', 'prot3'), ('prot2', 'prot4'), ('prot4', 'prot1'), ('prot3', 'prot4'), ('prot5', 'prot2'), ('prot1', 'prot5'), ('prot6', 'prot5'), ('prot2', 'prot6'), ('prot3', 'prot6')]
    for proteins in list_interactions:
        assert len(proteins) == 2


test_read_interaction_file_list()

def test_read_interaction_file_mat() -> None:
    """Tests the function read_interaction_file_mat
    
    Returns:
        None
    """
    matrix_interactions = read_interaction_file_mat("test_interactome_file.txt")
    assert len(matrix_interactions) == 2
    assert matrix_interactions == (['prot1', 'prot2', 'prot3', 'prot4', 'prot5', 'prot6'], [[0., 1., 1., 1., 1., 0.],
       [1., 0., 1., 1., 1., 1.],
       [1., 1., 0., 1., 0., 1.],
       [1., 1., 1., 0., 0., 0.],
       [1., 1., 0., 0., 0., 1.],
       [0., 1., 1., 0., 1., 0.]])

#test_read_interaction_file_mat()

def test_read_interaction_file() -> None:
    """Tests the function read_interaction_file

    Returns:
        _type_: None
    """
    print(read_interaction_file("test_interactome_file.txt"))
    assert type(read_interaction_file("test_interactome_file.txt")) == tuple
    assert type(read_interaction_file("test_interactome_file.txt")[0]) == dict
    assert read_interaction_file("test_interactome_file.txt")[0] == read_interaction_file_dict("test_interactome_file.txt")
    assert type(read_interaction_file("test_interactome_file.txt")[1]) == list
    assert read_interaction_file("test_interactome_file.txt")[1] == read_interaction_file_list("test_interactome_file.txt")
    assert type(read_interaction_file("test_interactome_file.txt")[2]) == np.ndarray
    assert type(read_interaction_file("test_interactome_file.txt")[3]) == list
    #assert read_interaction_file("test_interactome_file.txt")[4], read_interaction_file("test_interactome_file.txt")[5] == read_interaction_file_mat("test_interactome_file.txt")

test_read_interaction_file()

def test_count_vertices() -> None:
    """Tests the count_vertices function

    Returns:
        None
    """
    assert type(count_vertices("test_interactome_file.txt")) == int
    assert count_vertices("test_interactome_file.txt") == 6

test_count_vertices()
    

def test_count_edges():
    """Tests the count_edges function 
    """
    assert type(count_edges("test_interactome_file.txt")) == int
    assert count_edges("test_interactome_file.txt") == 11

test_count_edges()

def test_clean_interactome():
    """Tests the clean_interactome function
    """
    assert clean_interactome("test_bad_interactome_file.txt", "test_interactome_cleaned.txt") == None
    with open("test_interactome_cleaned.txt", 'r') as fh:
        line_counter = 0
        interactions = []
        for line in fh:
            if line_counter > 0:
                interaction = line.split(" ")
                interaction[-1] = interaction[-1].strip()
                protein1 = interaction[0]
                protein2 = interaction[1]
                # check if there are no homodimers
                assert protein1 != protein2
                interactions.append((protein1, protein2))
            line_counter += 1
    for interaction in interactions:
        prot1 = interaction[0]
        prot2 = interaction[1]
        # check if there are no homodimers
        assert prot1 != prot2
    # check if there are no duplicates
    assert len(interactions) == len(set(interactions))

test_clean_interactome()

def test_get_degree():
    """Tests the get_degree function
    """
    assert get_degree("test_interactome_file.txt", "prot1") == 4
    assert get_degree("test_interactome_file.txt", "prot2") == 5
    assert get_degree("test_interactome_file.txt", "prot3") == 4
    assert get_degree("test_interactome_file.txt", "prot4") == 3
    assert get_degree("test_interactome_file.txt", "prot5") == 3
    assert get_degree("test_interactome_file.txt", "prot6") == 3

test_get_degree()

def test_get_max_degree():
    """Tests the get_max_degree function
    """
    assert get_max_degree("test_interactome_file.txt") == [("prot2", 5)]

test_get_max_degree()

def test_get_ave_degree():
    """Tests the get_ave_degree function
    """
    assert get_ave_degree("test_interactome_file.txt") == 4

test_get_ave_degree()

def test_count_degree():
    """Tests the count_degree function
    """
    assert count_degree("test_interactome_file.txt", 3) == 3
    assert count_degree("test_interactome_file.txt", 4) == 2
    assert count_degree("test_interactome_file.txt", 5) == 1
    assert count_degree("test_interactome_file.txt", 12) == 0

test_count_degree()

def test_histogram_degree():
    """Tests the histogram_degree function
    """
    print(str(3) + " " + "***" + "\n" + str(4) + " " + "**" + "\n" + str(5) + " " + "*" + "\n")
    assert histogram_degree("test_interactome_file.txt", 3, 5) == str(3) + " " + "***" + "\n" + str(4) + " " + "**" + "\n" + str(5) + " " + "*"

test_histogram_degree()

if __name__ == '__main__':
    unittest.main()