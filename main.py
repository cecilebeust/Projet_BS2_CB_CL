'''
Projet UE BS2 - Master 2 Bioinformatique
parcours IBI

Corentin Lucas
Cécile Beust

MAIN FILE
'''

######## IMPORT MODULES ##########
from itertools import count
from webbrowser import get
from xxlimited import new
import numpy as np
import random
import matplotlib.pyplot as plt
import networkx as nx

########### CHAPTER 1 ############

# 1.2.1 : Store information from the input file in a dictonary

def read_interaction_file_dict(filename:str) -> dict:
    '''
    Function which returns a dictionary containing protein-protein interaction \
        stored in an input file
    
    :param filename : the name of the input file
    :type filename : str
    :return dico_interactions : a dictionary containing the proteins as keys and the interacting \
        proteins inside lists as values
    :rtype : dict
    '''
    dico_interactions:dict = {}
    with open(filename, 'r') as fh: # opening of the file
        line_counter:int = 0 # set a line counter to 0
        for line in fh: # browsing of the file
            if line_counter > 0: # skip the first line 
                current_line:str = line.split(" ") # split the line in two
                current_line[-1] = current_line[-1].strip() # remove \n
                # Store the 2 protein in variables
                protein1:str = current_line[0]
                protein2:str = current_line[1]
                # Update the dictionary
                dico_interactions.setdefault(protein1, []).append(protein2)
                dico_interactions.setdefault(protein2, []).append(protein1)
            # Update line counter
            line_counter += 1
        return dico_interactions

                        
#print(read_interaction_file_dict("toy_example.txt"))

# 1.2.2 : Store information from the input file in a list

def read_interaction_file_list(filename:str) -> list:
    '''
    Function which returns a list containing protein-protein interactions \
        stored in an input file

    :param filename : the name of the input file
    :type filename : str
    :return list_interactions : a list containing the protein-protein interactions \
        in the form of tuples
    :rtype : list
    '''
    list_interactions:list = []
    with open(filename, 'r') as fh:
        line_counter:int = 0
        for line in fh:
                if line_counter > 0:
                    current_line:str = line.split(" ")
                    current_line[-1] = current_line[-1].strip() # remove \n
                    protein1:str = current_line[0]
                    protein2:str = current_line[1]
                    # Update the list
                    if (protein2, protein1) not in list_interactions:  # Avoiding duplicates
                        list_interactions.append((protein1, protein2))
                # Update line counter
                line_counter += 1
        return list_interactions

#print(read_interaction_file_list("toy_example.txt"))

# 1.2.3 : Store protein-protein interactions in an adjacency matrix

def read_interaction_file_mat(filename:str) -> tuple:
    '''
    Returns an adjacency matrix of the protein-protein interactions stored \
        in an input file
    
    :param filename : the name of the input file
    :rtype filename : str
    :return list_summits : a list containing all the summits sorted alphabetically
    :rtype list_summits : list
    :return matrix : a numpy matrix describing protein-protein interactions. The matrix \
        contains a 1 if there is an interaction between the 2 proteins, and a 0 else.
    :rtype matrix : np.ndarray
    '''
    dico_interactions:dict = read_interaction_file_dict(filename)
    dim_matrix:int = len(dico_interactions)
    matrix = np.zeros(shape=(dim_matrix,dim_matrix))
    list_summits:list = sorted(list(dico_interactions.keys()))
    with open(filename, 'r') as fh:
        line_counter = 0
        for line in fh:
                if line_counter > 0:
                    current_line = line.split(" ")
                    current_line[-1] = current_line[-1].strip() # remove \n
                    protein1 = current_line[0]
                    protein2 = current_line[1]
                    # Search in the list of summits to what summit \
                    # the current proteins correspond to
                    for i in range(0, len(list_summits)):
                        if list_summits[i] == protein1:
                            # store the index of the first protein in the matrix
                            pos_summit_1 = i
                        elif list_summits[i] == protein2:
                            # store the index of the second protein in the matrix
                            pos_summit_2 = i
                    # Update the matrix
                    matrix[pos_summit_1, pos_summit_2] = 1
                    matrix[pos_summit_2, pos_summit_1] = 1
                line_counter += 1
    return list_summits, matrix


# 1.2.4

def read_interaction_file(filename:str) -> tuple:
    '''
    Function which returns the dictionary, the list, and the matrix all \
        containing protein-protein interactions from an input file and a list \
            of the summits

    :param filename : the name of the input file
    :type filename : str
    
    :return d_int : the dictionary containing the proteins as keys and \
        the interacting proteins inside lists as values
    :rtype d_int : dict

    :return l_int : the list contaning protein-protein interactions from \
        the inpurt file in the form of tuples
    :rtype l_int : list

    :return m_int : an adjacency matrix contaning 1 when the 2 proteins \
        are interactiong and a 0 else
    :rtype m_int : np.ndarray

    :return l_som : the list of all the summits sorted alphabetically
    :rtype l_som : list
    '''
    d_int:dict = read_interaction_file_dict(filename)
    l_int:list = read_interaction_file_list(filename)
    l_som, m_int = read_interaction_file_mat(filename)
    return d_int, l_int, m_int, l_som

#print(read_interaction_file("toy_example.txt"))

# 1.2.5 

# Avoid to store the whole content of the interaction file in a variable
# Avoid to browse the file multiple times

########## CHAPTER 2 ############

# 2.1.1 

def count_vertices(file) -> int:
    '''
    Function which counts the number of vertices of a graph

    :param file : the name of the input file
    :type file : str
    :return nb_vertices : the number of vertices in the graph
    :rtype nb_vertices : int

    appeler le dico
    '''
    vertices = []
    dico_interactions = read_interaction_file_dict(file)
    for protein in dico_interactions.keys():
        if protein not in vertices:
            vertices.append(protein)
    nb_vertices = len(vertices)
    return nb_vertices

#print(count_vertices("toy_example.txt"))

def count_edges(file) -> int:
    '''
    Function which counts the number of edge in a protein graph

    ;param file : the input file
    :type file : str
    :return nb_edge : the number of edges in the graph
    :rtype nb_edge : int
    '''
    list_interactions = read_interaction_file_list(file)
    nb_edges = len(list_interactions)
    return nb_edges

#print(count_edges("toy_example.txt"))

def clean_interactome(filein, fileout) -> None:
    '''
    Function which removes redundant interactions and homodimers \
        from an input file and writes the cleaned graph in an output file

    :param filein : the input file containing protein-protein interaction
    :type filein : str
    :param fileout : the name of the output file    
    :type fileout : str
    :returns None
    '''
    # open the input file in reading mode
    with open(filein, 'r') as fh:
        # open the output file in writing mode
        with open(fileout, 'w') as fo:
            line_counter = 0
            line_seen = []
            counter_interaction = 0
            # browsing of the input file
            for line in fh:
                # if it is not the first line
                if line_counter > 0:
                    # get the content of the line
                    interaction = line.split(" ")
                    interaction[-1] = interaction[-1].strip()
                    protein1 = interaction[0]
                    protein2 = interaction[1]
                    # if it is not an homodimer
                    if protein1 != protein2:
                        # if the interaction has not already been seen
                        if (protein1, protein2) not in line_seen:
                            if (protein2, protein1) not in line_seen:
                                counter_interaction += 1
                                # append it in the list of seen interactions
                                line_seen.append((protein1, protein2))
                                # write this interaction in the output file
                                fo.write(str(protein1) + " " + str(protein2) + "\n")
                # update line counter
                line_counter += 1
# problem line counter

#clean_interactome("redundante_file.txt", "cleaned_file.txt")

def get_degree(file, prot) -> int:
    '''
    Function which returns the degree of a protein in a graph

    :param file : the name of the input file
    :type file : str
    :param prot : the name of the protein
    :type prot : str
    :returns : the degree of the protein prot
    :rtype : int
    '''
    dico_interactions = read_interaction_file_dict(file)
    if prot in dico_interactions:
        interacting_proteins = dico_interactions[prot]
        degree = len(interacting_proteins)
    return degree

#print(get_degree("toy_example.txt", "B"))

def get_max_degree(file) -> list:
    '''
    Function which returns the protein of maximal degree \
        and its associated degree

    si on apelle get_degree on relit le fichier à chaque fois
    '''
    dico_interactions = read_interaction_file_dict(file)
    dico_degrees = {}
    res =  []
    for protein in dico_interactions:
        dico_degrees[protein] = get_degree(file, protein)
    for protein_degrees in dico_degrees:
        if dico_degrees[protein_degrees] == max(dico_degrees.values()):
            res.append((protein_degrees, dico_degrees[protein_degrees]))
    return res
    
#print(get_max_degree("toy_example.txt"))

def get_ave_degree(file):
    dico_interactions = read_interaction_file_dict(file)
    nb_deg = 0
    sum_deg = 0
    for protein in dico_interactions:
        nb_deg += 1
        sum_deg += get_degree(file, protein)
    return(round(sum_deg/nb_deg))

#print(get_ave_degree("toy_example.txt"))
#print(get_ave_degree("test_interactome_file.txt"))

def count_degree(file, deg):
    dico_interactions = read_interaction_file_dict(file)
    nb_prot_equal_deg = 0
    for protein in dico_interactions:
        deg_protein = get_degree(file, protein)
        if deg_protein == deg:
            nb_prot_equal_deg += 1
    return nb_prot_equal_deg

#print(count_degree("toy_example.txt", 2))

def histogram_degree(file, dmin, dmax):
    for deg in range(dmin, dmax+1):
        nb_prot_equal_deg = count_degree(file, deg)
        print(str(deg) + " " + "*"*nb_prot_equal_deg)

#print(histogram_degree("toy_example.txt", 1, 3))
#print(histogram_degree("test_interactome_file.txt", 3, 5))


def show_graph_with_labels(adjacency_matrix):
    rows, cols = np.where(adjacency_matrix == 1)
    edges = zip(rows.tolist(), cols.tolist())
    gr = nx.Graph()
    gr.add_edges_from(edges)
    nx.draw(gr, pos=nx.circular_layout(gr), node_size=500, labels=None, with_labels=True)
    plt.show()

#print("###################################################")

# Chapitre 3

#print("RANDOM GRAPH ERDOS")

'''def graph_genereator_erdos(n: int, p: float = 1):
    """AI is creating summary for graph_genereator_erdos

    Args:
        n (int): [description]
        p (float, optional): [description]. Defaults to 1.

    Returns:
        array: [description]
    """
    matrice = np.array([[0]*n]*n)
    cont1 = 0
    for i in matrice:
        cont2 = 0
        for j in i:
            if random.random() < p:
                matrice[cont1][cont2] = 1
            cont2 += 1
        cont1 += 1
    return matrice

mat = graph_genereator_erdos(n=20, p=0.08)'''

'''def show_graph_with_labels(adjacency_matrix, mylabels=None):
    rows, cols = np.where(adjacency_matrix == 1)
    edges = zip(rows.tolist(), cols.tolist())
    gr = nx.Graph()
    gr.add_edges_from(edges)
    nx.draw(gr, node_size=500, labels=None, with_labels=True)
    plt.show()
    nx.draw(gr, node_size=500, labels=None, with_labels=True)
    plt.show()

show_graph_with_labels(mat)'''

'''print("RANDOM GRAPH BARABSY")

def graph_generator_barabasy(n0: int = 2, nsup: int = 1):
    matrice = np.array([[1]*n0]*n0)
    for i in range(n0):
        matrice[i][i] = 0

    for _ in range(nsup):
        new_axis = [0]*(len(matrice[0]))
        for j in range(len(matrice[0])):
            if random.random() < (sum(matrice[j]/len(matrice[0]))):
                new_axis[j] = 1

        new_col = new_axis.copy()
        new_col.append(0)
        new_col = np.array(new_col)[:, np.newaxis]

        new_axis = np.array(new_axis)[np.newaxis, :]

        matrice = np.concatenate((matrice, new_axis))
        matrice = np.concatenate((matrice, new_col), axis=1)

    return matrice

mat = graph_generator_barabasy(n0=2, nsup=10)
print(mat)
mylabels = ["A", "B", "C", "D", "E"]'''






