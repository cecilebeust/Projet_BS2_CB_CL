# 1.2.1
import numpy as np


def read_interaction_file_dict(file: str):
    """_summary_

    Args:
        file (str): _description_

    Returns:
        _type_: _description_
    """
    dico = {}
    with open(file, "r") as fi:
        compt_line = 0
        for i in fi:
            if compt_line > 0:
                line_read = i.strip().split("\t")
                if line_read[0] in dico:
                    dico[line_read[0]].append(line_read[1])
                else:
                    dico[line_read[0]] = [line_read[1]]
                if line_read[1] in dico:
                    dico[line_read[1]].append(line_read[0])
                else:
                    dico[line_read[1]] = [line_read[0]]
            compt_line += 1
    return dico


# print(read_interaction_file_dict('./toy_example.txt'))

# 1.2.2


def read_interaction_file_list(file: str):
    """_summary_

    Args:
        file (str): _description_

    Returns:
        _type_: _description_
    """
    liste = []
    with open(file, "r") as fi:
        compt_line = 0
        for i in fi:
            if compt_line > 0:
                line_read = i.strip().split(" ")
                liste.append((line_read[0], line_read[1]))
            compt_line += 1
    liste = list(set(liste))
    return liste


print(read_interaction_file_list('./toy_example.txt'))

# 1.2.3


def read_interaction_file_mat(file: str):
    """_summary_

    Args:
        file (str): _description_

    Returns:
        _type_: _description_
    """
    dico = {}
    with open(file, "r") as fi:
        compt_line = 0
        for i in fi:
            if compt_line > 0:
                line_read = i.strip().split(" ")
                if line_read[0] not in dico:
                    dico[line_read[0]] = len(dico)
                if line_read[1] not in dico:
                    dico[line_read[1]] = len(dico)
            else:
                compt_line = 1
    matrice = np.array([[0]*len(dico)]*len(dico))

    compt_line = 0
    with open(file, "r") as fi:
        for j in fi:
            if compt_line > 0:
                line_read = j.strip().split(" ")
                matrice[dico[line_read[0]]][dico[line_read[1]]] = 1
                matrice[dico[line_read[1]]][dico[line_read[0]]] = 1
            compt_line = 1

    return matrice


print(read_interaction_file_mat('./toy_example.txt'))

# 1.2.4


def read_interaction_file(file: str):
    """_summary_

    Args:
        file (str): _description_

    Returns:
        _type_: _description_
    """
    l_som = []
    with open(file, "r") as fi:
        compt_line = 0
        for i in fi:

            if compt_line > 0:
                line_read = i.split(" ")
                print(line_read)
                if line_read[0] not in l_som:
                    l_som.append(line_read[0])
                if line_read[1].strip() not in l_som:
                    l_som.append(line_read[1].strip())
            compt_line = 1
    return (read_interaction_file_dict(file), read_interaction_file_list(file), read_interaction_file_mat(file), l_som)


# print(read_interaction_file('./Human_HighQuality.txt'))

# 1.2.5
# ??

# 1.2.6

# 1.2.7

def is_interaction_file(file: str):
    """_summary_

    Args:
        file (str): _description_

    Returns:
        _type_: _description_
    """
    with open(file, "r") as fi:
        if len(list(fi)) == 0:
            return False
        compt_line = 0
        for i in fi:
            if compt_line > 0:
                if len(i.strip().split("\t")) != 2:
                    return False
            else:
                if not i.strip().isdigit():
                    return False
                compt_line = 1
    return True


print(is_interaction_file("./schema.txt"))

# 2.1
# 2.2.1


def count_vertices(file: str):
    return len(read_interaction_file_dict(file))


print(count_vertices("./Human_HighQuality.txt"))
