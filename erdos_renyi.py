import matplotlib.pyplot as plt
import networkx as nx
import numpy as np
import random


def graph_genereator_erdos(n: int, p: float = 1):
    """AI is creating summary for graph_genereator_erdos

    Args:
        n (int): [description]
        p (float, optional): [description]. Defaults to 1.

    Returns:
        array: [description]
    """
    matrice = np.array([[0]*n]*n)
    cont1 = 0
    for i in matrice:
        cont2 = 0
        for j in i:
            if random.random() < p:
                matrice[cont1][cont2] = 1
            cont2 += 1
        cont1 += 1
    return matrice


mat = graph_genereator_erdos(n=20, p=0.08)


def show_graph_with_labels(adjacency_matrix, mylabels=None):
    rows, cols = np.where(adjacency_matrix == 1)
    edges = zip(rows.tolist(), cols.tolist())
    gr = nx.Graph()
    gr.add_edges_from(edges)
    nx.draw(gr, node_size=500, labels=None, with_labels=True)
    plt.show()
    nx.draw(gr, node_size=500, labels=None, with_labels=True)
    plt.show()


show_graph_with_labels(mat)
